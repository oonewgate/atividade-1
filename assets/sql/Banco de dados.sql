-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 10-Mar-2019 às 04:56
-- Versão do servidor: 10.1.35-MariaDB
-- versão do PHP: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ativ1`
--

CREATE DATABASE ativ1;
-- --------------------------------------------------------

--
-- Estrutura da tabela `cliente`
--

CREATE TABLE `cliente` (
  `email` varchar(30) NOT NULL,
  `nome` varchar(30) NOT NULL,
  `titulo` varchar(20) NOT NULL,
  `preco` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `cliente`
--

INSERT INTO `cliente` (`email`, `nome`, `titulo`, `preco`) VALUES
('edson@gmail.com', 'Edson', 'Plano Básico', 20),
('igor_keiki@outlook.com', 'Igor Keiki', 'Plano Premium', 38),
('pietro@gmail.com', 'Pietro', 'Plano Padrão', 28);

-- --------------------------------------------------------

--
-- Estrutura da tabela `contato`
--

CREATE TABLE `contato` (
  `id` tinyint(2) NOT NULL,
  `nome` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `motivo` varchar(20) NOT NULL,
  `mensagem` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `contato`
--

INSERT INTO `contato` (`id`, `nome`, `email`, `motivo`, `mensagem`) VALUES
(1, 'Igor', 'igor_keiki@outlook.com', '1', 'Os filmes de Harry Potter estão no catálogo?'),
(2, 'Diego', 'diegokazuo@gmail.com', '2', 'Realizei o pagamento da mensalidade e mesmo assim minha conta foi bloqueada, o que fazer?');

-- --------------------------------------------------------

--
-- Estrutura da tabela `empresa`
--

CREATE TABLE `empresa` (
  `titulo` varchar(30) NOT NULL,
  `texto` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `empresa`
--

INSERT INTO `empresa` (`titulo`, `texto`) VALUES
('Netflix Inc.', 'A Netflix foi fundada por Reed Hastings e Marc Randolph em 1997. Na época, a empresa trabalhava com locação de filmes, mas um ano mais tarde o serviço passou a oferecer entrega de DVDs pelos Correios através do site da companhia. Atualmente, a Netflix oferece filmes e séries por streaming e conta com cerca de 117 milhões de assinaturas ao redor do mundo.<br /><br />O modelo atual de negócio teve início em 2007. Na plataforma, os assinantes pagam uma mensalidade e têm acesso a milhares de títulos 24 horas por dia. Os conteúdos podem ser acessados em TVs conectadas, smartphones, tablets e videogames. Além da transmissão de filmes e séries de terceiros, desde 2013 a Netflix vem produzindo conteúdos originais, como House of Cards, Orange is the New Black, Stranger Things, Narcos e 13 Reasons Why.');

-- --------------------------------------------------------

--
-- Estrutura da tabela `homepage`
--

CREATE TABLE `homepage` (
  `titulo` varchar(30) NOT NULL,
  `subtitulo` varchar(100) NOT NULL,
  `descr` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `homepage`
--

INSERT INTO `homepage` (`titulo`, `subtitulo`, `descr`) VALUES
('Sobre Netflix', 'A Netflix é uma das líderes no serviço de conteúdo digital desde 1997.', 'A Netflix é o principal serviço de entretenimento por internet do mundo. São 139 milhões de assinaturas em mais de 190 países assistindo a séries, documentários e filmes de diversos gêneros e idiomas. O assinante Netflix pode assistir a quantos filmes e séries quiser, quando e onde quiser, em praticamente qualquer tela com conexão à internet. O assinante pode assistir, pausar e voltar a assistir a um título sem comerciais e sem compromisso.');

-- --------------------------------------------------------

--
-- Estrutura da tabela `plano`
--

CREATE TABLE `plano` (
  `id` tinyint(2) NOT NULL,
  `titulo` varchar(20) NOT NULL,
  `descr` varchar(200) NOT NULL,
  `preco` int(3) NOT NULL,
  `imagem` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `plano`
--

INSERT INTO `plano` (`id`, `titulo`, `descr`, `preco`, `imagem`) VALUES
(1, 'Plano Básico', '- Telas simultâneas: 1<br />- Compatibilidade com notebook, TV, smartphone ou tablet<br />- Filmes e séries ilimitados<br />- Cancele quando quiser', 20, '../../assets/img/CardImageBasico.png'),
(2, 'Plano Padrão', '- HD disponível<br />- Telas simultâneas: 2<br />- Compatibilidade com notebook, TV, smartphone ou tablet<br />- Filmes e séries ilimitados<br />- Cancele quando quiser', 28, '../../assets/img/CardImagePadrao.png'),
(3, 'Plano Premium', '-Ultra HD disponível<br />- Telas simultâneas: 4<br />- Compatibilidade com notebook, TV, smartphone ou tablet<br />- Filmes e séries ilimitados<br />- Cancele quando quiser', 38, '../../assets/img/CardImagePremium.png');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`email`);

--
-- Indexes for table `contato`
--
ALTER TABLE `contato`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `plano`
--
ALTER TABLE `plano`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contato`
--
ALTER TABLE `contato`
  MODIFY `id` tinyint(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `plano`
--
ALTER TABLE `plano`
  MODIFY `id` tinyint(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
