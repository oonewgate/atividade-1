<div class="d-flex justify-content-center mt-3">
   <!-- Card -->
    <div class="card col-md-4 black" id="card">
    <!-- Card image -->
    <img class="card-img-top" src="<?= $imagem ?>" alt="Card image">
    <!-- Card content -->
    <div class="card-body text-white">
        <!-- Title -->
        <h4 class="card-title">
            <a>
                <?= $titulo ?>
                <br />
                Valor mensal: R$<?= $preco ?>,00
            </a>
        </h4>
        <!-- Text -->
        <p class="card-text text-white"><?= $descr ?></p>
        <!-- Button -->
        <div class="text-center">
            <a href="http://localhost/atividade01/netflix/assinatura/<?= $id?>" class="btn red">Assinar</a>
        </div>
    </div>
    </div>
    <!-- Card -->
</div>