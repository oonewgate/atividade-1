<div class="container" id="jumbo">
    <div class="jumbotron z-depth-3 rounded mt-120">
        <h2 class="display-4"><?= $titulo ?></h2>
        <p class="lead"><?= $subtitulo ?></p>
        <hr class="my-4">
        <p><?= $descr ?></p>
    </div>
</div>