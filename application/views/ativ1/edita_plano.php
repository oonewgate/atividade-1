<!-- Default form contact -->
<div class="container d-flex justify-content-center">
    <form method="POST" class="text-center z-depth-1-half p-5 rounded mb-0 elegant-color-dark mt-4 mb-4 col-md-6" id="form">

        <p class="h4 mb-4 text-light">Editar <?= $titulo ?></p>

        <!-- Name -->
        <input type="text" value="<?= isset($titulo) ? $titulo: '' ?>" id="titulo" name="titulo" class="form-control mb-4" placeholder="Título">

        <!-- Image -->
        <input type="text" value="<?= isset($imagem) ? $imagem: '' ?>" id="imagem" name="imagem" class="form-control mb-4" placeholder="Imagem">

        <!-- Email -->
        <input type="text" value="<?= isset($preco) ? $preco: '' ?>" id="preco" name="preco" class="form-control mb-4" placeholder="Preço">

        <!-- Message -->
        <div class="form-group">
            <textarea class="form-control rounded-0" id="descr" name="descr" rows="3" placeholder="Descrição"><?= isset($descr) ? $descr: '' ?></textarea>
        </div>

        <!-- Send button -->
        <button class="btn btn-block red darken-4" type="submit">Salvar</button>

    </form>
</div>
<!-- Default form contact -->