<!-- Default form contact -->
<div class="container d-flex justify-content-center">
    <form method="POST" class="text-center z-depth-1-half p-5 rounded mb-0 elegant-color-dark mt-4 mb-4 col-md-6" id="form">

        <p class="h4 mb-4 text-light">Finalizar compra</p>

        <!-- Name -->
        <input type="text" id="nome" name="nome" class="form-control mb-4" placeholder="Nome">

        <!-- Email -->
        <input type="text" id="email" name="email" class="form-control mb-4" placeholder="E-mail">

        <!-- Subject -->
        <select  class="browser-default custom-select mb-4" id="titulo" name="titulo">
            <option value="<?= $titulo ?>" selected><?= $titulo ?></option>
        </select>
        <select  class="browser-default custom-select mb-4" id="preco" name="preco">
            <option value="<?= $preco ?>" selected>R$<?= $preco ?>,00 mensais</option>
        </select>

        <!-- Send button -->
        <button class="btn btn-block red darken-4" type="submit">Assinar</button>

    </form>
</div>
<!-- Default form contact -->