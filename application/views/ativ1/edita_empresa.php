<!-- Default form contact -->
<div class="container d-flex justify-content-center">
    <form method="POST" class="text-center z-depth-1-half p-5 rounded mb-0 elegant-color-dark mt-4 mb-4 col-md-6" id="form">

        <p class="h4 mb-4 text-light">Editar conteúdo da seção "Empresa"</p>

        <!-- Name -->
        <input type="text" value="<?= isset($titulo) ? $titulo: '' ?>" id="titulo" name="titulo" class="form-control mb-4" placeholder="Título">

        <!-- Message -->
        <div class="form-group">
            <textarea class="form-control rounded-0" id="texto" name="texto" rows="6" placeholder="Texto"><?= isset($texto) ? $texto: '' ?></textarea>
        </div>

        <!-- Send button -->
        <button class="btn btn-block red darken-4" type="submit">Salvar</button>

    </form>
</div>
<!-- Default form contact -->