<!-- Default form contact -->
<div class="container d-flex justify-content-center">
    <form method="POST" class="text-center z-depth-1-half p-5 rounded mb-0 elegant-color-dark mt-4 mb-4 col-md-6" id="form">

        <p class="h4 mb-4 text-light">Contato</p>

        <!-- Name -->
        <input type="text" id="nome" name="nome" class="form-control mb-4" placeholder="Nome">

        <!-- Email -->
        <input type="email" id="email" name="email" class="form-control mb-4" placeholder="E-mail">

        <!-- Subject -->
        <label class="text-light">Motivo do contato</label>
        <select id="motivo" name="motivo" class="browser-default custom-select mb-4">
            <option value="" disabled selected>Escolha uma opção</option>
            <option value="1">Dúvidas</option>
            <option value="2">Problemas com pagamento</option>
            <option value="3">Outro</option>
        </select>

        <!-- Message -->
        <div class="form-group">
            <textarea class="form-control rounded-0" id="mensagem" name="mensagem" rows="3" placeholder="Mensagem"></textarea>
        </div>

        <!-- Send button -->
        <button class="btn btn-block red darken-4" type="submit">Enviar</button>

    </form>
</div>
<!-- Default form contact -->