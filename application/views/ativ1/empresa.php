<div class="container" id="jumbo">
    <div class="jumbotron z-depth-3 rounded mt-120">
        <h2 class="display-4"><?= $titulo ?></h2>
        <hr class="my-4">
        <p><?= $texto ?></p>
        <p>Site oficial: <a href="https://www.netflix.com/br/" target="_blank">https://www.netflix.com/br/</a></p>
    </div>
</div>