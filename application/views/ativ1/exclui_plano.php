<div class="container d-flex justify-content-center">
    <div class="z-depth-1-half p-5 rounded mb-0 elegant-color-dark mt-4 mb-4 col-md-6 text-white m-5" id="card">
        <h3>Deseja mesmo excluir <?= $titulo ?>?</h3>
        <form method="post" class="pt-3">
            <input type="radio"  name="opc" value="sim" checked> Sim<br />
            <input type="radio" id="opc" name="opc" value="nao"> Não
            <div class="text-center pt-3">
                <input class="btn red" type="submit" value="Confirmar">
                <a href='http://localhost/atividade01/netflix/home/<?= $id?>' class='btn red'>Voltar</a>
            </div>
            <?php

                if(isset($_POST['opc'])){
                    $opc = $_POST['opc'];

                    if($opc == 'sim'){
                        $this->NetflixModel->excluir_plano($id);
                        echo "<p class='text-center'>Plano excluído com sucesso</p>";
                    }
                }
            ?>
        </form>
    </div>
</div>