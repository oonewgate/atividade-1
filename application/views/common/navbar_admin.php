<?php
  include_once("conexao.php");
?>

<!--Navbar -->
<nav class="mb-1 navbar navbar-expand-lg navbar-dark elegant-color-dark z-depth-1-half">
  <img src="<?= base_url('assets/img/Logo.png') ?>" alt="Netflix logo" width="5%"/>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-333"
    aria-controls="navbarSupportedContent-333" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent-333">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="http://localhost/atividade01/netflix/edita_homepage">Editar home</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="http://localhost/atividade01/netflix/edita_empresa">Editar empresa</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="http://localhost/atividade01/netflix/novo_plano">Criar plano</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink-333" data-toggle="dropdown" aria-haspopup="true"
          aria-expanded="false">Editar
        </a>
        <div class="dropdown-menu dropdown-default" aria-labelledby="navbarDropdownMenuLink-333">
          <?php
            $result = "SELECT * FROM plano";
            $resultado = mysqli_query($connection, $result);
            while($row = mysqli_fetch_assoc($resultado)){
              echo "<a class='dropdown-item' href='http://localhost/atividade01/netflix/edita_plano/$row[id]'>". $row['titulo'] ."</a>";
            }
          ?>
        </div>
      </li>

      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink-333" data-toggle="dropdown" aria-haspopup="true"
          aria-expanded="false">Excluir
        </a>
        <div class="dropdown-menu dropdown-default" aria-labelledby="navbarDropdownMenuLink-333">
          <?php
            $result = "SELECT * FROM plano";
            $resultado = mysqli_query($connection, $result);
            while($row = mysqli_fetch_assoc($resultado)){
              echo "<a class='dropdown-item' href='http://localhost/atividade01/netflix/exclui_plano/$row[id]'>". $row['titulo'] ."</a>";
            }
          ?>
        </div>
      </li>
    </ul>
    

    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="http://localhost/atividade01/netflix/home">Usuário</a>
      </li>
    </ul>
  </div>
</nav>
<!--/.Navbar -->