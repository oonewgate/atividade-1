<?php
  include_once("conexao.php");
?>

<!--Navbar -->
<nav class="mb-1 navbar navbar-expand-lg navbar-dark elegant-color-dark z-depth-1-half">
  <img src="<?= base_url('assets/img/Logo.png') ?>" alt="Netflix logo" width="5%"/>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-333"
    aria-controls="navbarSupportedContent-333" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent-333">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="http://localhost/atividade01/netflix/home">Home</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="http://localhost/atividade01/netflix/empresa">Empresa</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="http://localhost/atividade01/netflix/contato">Contato</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink-333" data-toggle="dropdown" aria-haspopup="true"
          aria-expanded="false">Planos
        </a>
        <div class="dropdown-menu dropdown-default" aria-labelledby="navbarDropdownMenuLink-333">
          <?php
            $result = "SELECT * FROM plano";
            $resultado = mysqli_query($connection, $result);
            while($row = mysqli_fetch_assoc($resultado)){
              echo "<a class='dropdown-item' href='http://localhost/atividade01/netflix/planos/$row[id]'>". $row['titulo'] ."</a>";
            }
          ?>
        </div>
      </li>
    </ul>
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="http://localhost/atividade01/netflix/home_admin">Administração</a>
      </li>
    </ul>
  </div>
</nav>
<!--/.Navbar -->