<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    class NetflixModel extends CI_Model {

        public function get_data($id){
            $sql = "SELECT * FROM plano WHERE id = $id";
            $res = $this->db->query($sql);
            $data = $res->result_array();
            return $data[0];
        }

        public function get_data_from_homepage(){
            $sql = "SELECT * FROM homepage";
            $res = $this->db->query($sql);
            $data = $res->result_array();
            return $data[0];
        }

        public function get_data_from_empresa(){
            $sql = "SELECT * FROM empresa";
            $res = $this->db->query($sql);
            $data = $res->result_array();
            return $data[0];
        }

        public function novo_contato(){
            if(sizeof($_POST) == 0)
                return;

            $data = $this->input->post();
            $this->db->insert('contato', $data);
        }

        public function novo_plano(){
            if(sizeof($_POST) == 0)
                return;

            $data = $this->input->post();
            $this->db->insert('plano', $data);
        }

        public function editar_homepage(){
            $newdata = $this->input->post();

            if($newdata == null) return;
            $this->db->update('homepage', $newdata);
        }

        public function editar_empresa(){
            $newdata = $this->input->post();

            if($newdata == null) return;
            $this->db->update('empresa', $newdata);
        }

        public function editar_plano($id){
            $newdata = $this->input->post();

            if($newdata == null) return;
            $this->db->update('plano', $newdata, array('id' => $id));
        }

        public function nova_assinatura(){
            if(sizeof($_POST) == 0)
                return;

            $data = $this->input->post();
            $this->db->insert('cliente', $data);
        }

        public function excluir_plano($id){
            $this->db->where('id', $id);
            $this->db->delete('plano');
        }
    }
?>