<?php
    class Netflix extends CI_Controller{

        public function home(){
            $this->load->view('common/header');
            $this->load->view('common/navbar');
            $this->load->model('NetflixModel');
            $data = $this->NetflixModel->get_data_from_homepage();

            $v['titulo'] = $this->load->view('ativ1/home', $data, true);
            $v['subtitulo'] = $this->load->view('ativ1/home', $data, true);
            $v['descr'] = $this->load->view('ativ1/home', $data, true);

            $this->load->view('ativ1/home');
            $this->load->view('common/rodape');
            $this->load->view('common/footer');
        }

        public function home_admin(){
            $this->load->view('common/header');
            $this->load->view('common/navbar_admin');
            $this->load->model('NetflixModel');
            $data = $this->NetflixModel->get_data_from_homepage();

            $v['titulo'] = $this->load->view('ativ1/home', $data, true);
            $v['subtitulo'] = $this->load->view('ativ1/home', $data, true);
            $v['descr'] = $this->load->view('ativ1/home', $data, true);

            $this->load->view('ativ1/home');
            $this->load->view('common/rodape');
            $this->load->view('common/footer');
        }

        public function empresa(){
            $this->load->view('common/header');
            $this->load->view('common/navbar');
            $this->load->model('NetflixModel');
            $data = $this->NetflixModel->get_data_from_empresa();

            $v['titulo'] = $this->load->view('ativ1/empresa', $data, true);
            $v['texto']  = $this->load->view('ativ1/empresa', $data, true);

            $this->load->view('ativ1/empresa');
            $this->load->view('common/rodape');
            $this->load->view('common/footer');
        }

        public function planos($id){
            $this->load->view('common/header');
            $this->load->view('common/navbar');
            $this->load->model('NetflixModel');

            $data = $this->NetflixModel->get_data($id);
            $v['titulo'] = $this->load->view('ativ1/planos', $data, true);
            $v['descr']  = $this->load->view('ativ1/planos', $data, true);
            $v['preco']  = $this->load->view('ativ1/planos', $data, true);
            $v['imagem']  = $this->load->view('ativ1/planos', $data, true);

            $this->load->view('ativ1/planos');
            $this->load->view('common/rodape');
            $this->load->view('common/footer');
        }

        public function contato(){
            $this->load->view('common/header');
            $this->load->view('common/navbar');
            $this->load->model('NetflixModel');
            $this->NetflixModel->novo_contato();
            $this->load->view('ativ1/contato');
            $this->load->view('common/rodape');
            $this->load->view('common/footer');
        }

        public function edita_homepage(){
            $this->load->view('common/header');
            $this->load->view('common/navbar_admin');
            $this->load->model('NetflixModel');
            $this->NetflixModel->editar_homepage();
            $data = $this->NetflixModel->get_data_from_homepage();
            $this->load->view('ativ1/edita_homepage', $data);
            $this->load->view('common/rodape');
            $this->load->view('common/footer');
        }

        public function edita_empresa(){
            $this->load->view('common/header');
            $this->load->view('common/navbar_admin');
            $this->load->model('NetflixModel');
            $this->NetflixModel->editar_empresa();
            $data = $this->NetflixModel->get_data_from_empresa();
            $this->load->view('ativ1/edita_empresa', $data);
            $this->load->view('common/rodape');
            $this->load->view('common/footer');
        }

        public function novo_plano(){
            $this->load->view('common/header');
            $this->load->view('common/navbar_admin');
            $this->load->model('NetflixModel');
            $this->NetflixModel->novo_plano();
            $this->load->view('ativ1/novo_plano');
            $this->load->view('common/rodape');
            $this->load->view('common/footer');
        }

        public function edita_plano($id){
            $this->load->view('common/header');
            $this->load->view('common/navbar_admin');
            $this->load->model('NetflixModel');
            $this->NetflixModel->editar_plano($id);
            $data = $this->NetflixModel->get_data($id);
            $this->load->view('ativ1/edita_plano', $data);
            $this->load->view('common/rodape');
            $this->load->view('common/footer');
        }

        public function exclui_plano($id){
            $this->load->view('common/header');
            $this->load->view('common/navbar_admin');
            $this->load->model('NetflixModel');
            $data = $this->NetflixModel->get_data($id);
            $v['titulo'] = $this->load->view('ativ1/exclui_plano', $data, true);
            $this->load->view('ativ1/exclui_plano', $data);
            $this->load->view('common/rodape');
            $this->load->view('common/footer');
        }
        
        public function assinatura($id){
            $this->load->view('common/header');
            $this->load->view('common/navbar');
            $this->load->model('NetflixModel');
            $data = $this->NetflixModel->get_data($id);
            $this->NetflixModel->nova_assinatura($id);
            $this->load->view('ativ1/assinatura', $data);
            $this->load->view('common/rodape');
            $this->load->view('common/footer');
        }
    }
?>